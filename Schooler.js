import { Person } from "./Person.js";

class Schooler extends Person{
    constructor(name, dateBorn, address, schoolName, className, phone){
        super(name, dateBorn, address);
        this.schoolName = schoolName;
        this.className = className;
        this.phone = phone;
    }
}
export {Schooler};