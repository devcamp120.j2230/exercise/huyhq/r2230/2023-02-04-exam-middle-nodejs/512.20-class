import { Schooler } from "./Schooler";

class Student extends Schooler{
    constructor(name, dateBorn, address, 
        schoolName, className, phone,
        majors, mssv
        ){
        super(name, dateBorn, address, 
            schoolName, className, phone);
        this.majors = majors;
        this.mssv = mssv;
    }
}

export {Student};