import { Person } from "./Person.js";

class Worker extends Person{
    constructor(name, dateBorn, address, workName, workAddress, salary){
        super(name, dateBorn, address);
        this.workName = workName;
        this.workAddress = workAddress;
        this.salary = salary;
    }
}
export {Worker};